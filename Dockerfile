FROM debian:latest
RUN apt-get update && apt-get install -y cowsay fortune && rm -rf /var/lib/apt/lists/*

COPY entrypoint.sh /
USER 1001
ENTRYPOINT ["/entrypoint.sh"]

